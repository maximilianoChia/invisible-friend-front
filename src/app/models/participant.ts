import { assign } from "lodash"

export class ParticipantSchema {
  public name: string | undefined;
  public email: string | undefined;
  public firstDiscard: string | undefined;
  public secondDiscard: string | undefined;
  public invisibleFriend: string | undefined;
}

export class Participant extends ParticipantSchema {
  constructor(attributes?: ParticipantSchema) {
    super();
    assign(this, attributes);
  }
}

/*  Creo el schema que luego es heredado por la clase Participant
    El constructor de Participant recibe como parametro al schema
    luego por medio de super() llamo a los propiedades de la clase padre
    con assing se asignas esas propiedades heredadas al parametro del constructor
*/

