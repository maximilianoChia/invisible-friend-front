import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddParticipantFormComponent } from './components/add-participant-form/add-participant-form.component';

const routes: Routes = [
  { path: '', component: AddParticipantFormComponent },
  // { path: 'edit-participant/:id', component: formModal },
  { path: '**', redirectTo: '', pathMatch: 'full' } //If the request route does not exist, Will redirect to the main route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
