import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Participant } from 'src/app/models/participant';

@Component({
  selector: 'app-add-participant-form',
  templateUrl: './add-participant-form.component.html',
  styleUrls: ['./add-participant-form.component.css']
})
export class AddParticipantFormComponent implements OnInit {

  participantsForm: FormGroup;
  participant: Participant | undefined;

  constructor(private fb: FormBuilder) {
    this.participantsForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      firstDiscard: [''],
      secondDiscard: [''],
      invisibleFriend: [''],
    });
  }

  ngOnInit(): void {
  }

  addParticipant(): void {
    this.participant = {
      name: this.participantsForm.get('name')?.value,
      email: this.participantsForm.get('email')?.value,
      firstDiscard: '',
      secondDiscard: '',
      invisibleFriend: ''
    }
  }

}
